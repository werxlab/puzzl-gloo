![alt PuzzlGloo](public/images/logo/logo-v1_240-80.png)

# PuzzlGloo

is a ***Blog/CMS*** built using the [Symfony Framework](https://symfony.com). This project has been started with the 5.1.17 version of Symfony.

## Planned Features
* Full i18n capabilities
  * English (default)
  * German (secondary)
  * easily add more languages
* Blog
* Easy configuration
* Admin Panel
* <strike>User Administration</strike>
* Cookie Popup Notice

## Features (already implemented)
* Interface languages
  * English (default)
  * German (secondary)
* Langauge Switcher
* User Administration
  * uses database to store User
  * built from our FOSUserBundle fork, [WXLUserBundle](https://gitlab.com/werxlab/wxluserbundle)
* Sidebar
  * Some content implemented
* Contact Page

## Documentation
Some shorr tutorials in the [wiki](../../wikis), such as how to get started.

More tutorials on Symfony and our Bundles soon to come on our [Symfony Projects site](https://werxlab.org).

## License
***BSD 3-Clause "New" or "Revised" License***

A permissive license similar to the BSD 2-Clause License, but with a 3rd clause that prohibits others from using the name of the project or its contributors to promote derived products without written consent.

See [LICENSE](LICENSE) for details.

**Permissions**
* Commercial use
* Modification
* Distribution
* Private use

**Limitations**
* No Liability
* No Warranty

**Conditions**
* License and copyright notice
  * -must be included with the software.

## Contributing

If you want to help out and contibute to the project please read the [CONTRIBUTING file](CONTRIBUTING.md).
