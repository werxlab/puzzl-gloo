<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()
         ->getManager();

        $blogs = $em->getRepository('App:Blog')
                  ->getLatestBlogs();

        return $this->render('default/index.html.twig', array(
          'blogs' => $blogs
        ));
    }

    /**
     * @Route("/sidebar", name="sidebar")
     */
    public function sidebar(): Response
    {
        return $this->render('sidebar/index.html.twig', [
            'controller_name' => 'DefaultController',
        ]);
    }
}
